import { Container } from './styles';

import { Button } from '../../components/Button';

export function Details() {
	return (
		<Container>
			<h1>Hello World!</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero quia voluptates rem necessitatibus corporis maiores reprehenderit, ducimus perspiciatis eos aut adipisci culpa fuga ex officia. Magnam incidunt impedit similique fuga!</p>

			<Button label="Entrar" loading/>
			<Button label="Cadastrar"/>
			<Button label="Voltar"/>
		</Container>
	)
}