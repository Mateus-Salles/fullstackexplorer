require('express-async-errors');

const AppError = require('./utils/AppError');
const express = require('express');

const routes = require('./routes');

const app = express();
app.use(express.json());

app.use(routes);

app.use(( error, request, response, next ) => {
    if (error instanceof AppError) {
        return response.status(error.statusCode).json({
            status: "Error",
            message: error.message,
        });
    }

    console.error(error);

    return response.status(500).json({
        status: "Error",
        message: "Internal server error"
    })
});

app.listen(3333, () => console.log(`Server is running on Port ${3333}`));