const screen1 = window.document.querySelector('.screen1');
const screen2 = window.document.querySelector('.screen2');

const numb = window.document.querySelector('#number');
const verifyButton = window.document.querySelector('#verify-button');
const returnButton = window.document.querySelector('#play-again');
const attempts = window.document.querySelector('#attempts');

const machineNumber = Math.floor(Math.random() * 10);

var attemptsNumber = 0;

verifyButton.addEventListener('click', () => {
    attemptsNumber++;

    if (Number.isInteger(Number(numb.value)) && Number.parseInt(numb.value) <= 10) {
        let enteredNumber = Number.parseInt(numb.value);

        verify(enteredNumber);
    } else {
        alert('Escolha somente números que sejam inteiros entre 0 e 10');
    }
});

returnButton.addEventListener('click', () => {
    location.reload();
});

function verify(num) {
    if (num == machineNumber) {
        screen1.classList.add('hide');
        screen2.classList.remove('hide');
    }

    attempts.textContent = String(attemptsNumber);
}